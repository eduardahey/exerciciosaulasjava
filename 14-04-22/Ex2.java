import java.util.Scanner;
public class Ex2{
    public static Scanner leia = new Scanner(System.in);
    public static void main(String[] args){
        int erros = 0;
        while(erros < 3){
            System.out.print("Digite o seu usuário:");
            String usu = leia.next();
            System.out.print("Digite o sua senha:");
            String snh = leia.next();
            if((usu.equals("admin")) && (snh.equals("c3c"))){
                System.out.println("Usuário conseguiu logar.");
                break;
            }else{
                erros += 1;
            }
            if (erros == 3){
                System.out.println("Usuário não conseguiu logar.");
            }
        }
    }
}
