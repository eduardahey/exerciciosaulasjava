import java.util.Scanner;
public class Ex2{
    public static Scanner leia = new Scanner(System.in);
    public static void main(String[] args){
        float valor = 0;
        float custo = 0; 
        System.out.print("Número de maçãs compradas:");
        int compradas = leia.nextInt();
        if(compradas < 12){
            valor = 2.50f;
        }else{
            valor = 1.90f;
        }
        custo = compradas*valor;
        System.out.print(custo);
    }
}