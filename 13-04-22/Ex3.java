import java.util.Scanner;
public class Ex3{
    public static Scanner leia = new Scanner(System.in);
    public static void main(String[] args){
        int duracao = 0;
        System.out.print("Informe a hora de início do jogo:");
        int ini = leia.nextInt();
        System.out.print("Informe a hora de fim do jogo:");
        int fim = leia.nextInt();
        if((24 >= ini && ini > 0) && (24 >= fim && fim > 0)){
            if (fim > ini){
                duracao = fim-ini;
            }else{
                duracao = fim + (24-ini); 
            }
        }      
        if(duracao > 24){
            System.out.println("O tempo de jogo passou de 24 horas.");
        }
        else{
            System.out.println("O tempo do jogo é de:" + duracao);
        }
    }
}