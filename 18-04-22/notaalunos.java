import java.util.Scanner;
public class notaalunos {
    public static Scanner leia = new Scanner(System.in);
    public static void main(String[] args){
        float media[] = new float[5];
        float notas[] = new float[10];
        for(int i = 0; i < 5; i++){
            int n = i+1;
            System.out.print("Digite a primeira nota do aluno número " + n + ":");
            float n1 = leia.nextFloat();
            while((n1 < 0) || (n1 > 10)){
                System.out.print("Nota inválida. Digite a primeira nota novamente:");
                n1 = leia.nextFloat();
            }
            System.out.print("Digite a segunda nota do aluno número " + n + ":");
            float n2 = leia.nextFloat();
            while((n2 < 0) || (n2 > 10)){
                System.out.print("Nota inválida. Digite a segunda nota novamente:");
                n2 = leia.nextFloat();
            }
            notas [i] = n1;
            notas [n] = n2;
            media [i] = (notas[i]+notas[n])/2;
            if(i == 4){
                for(int x = 0; x < media.length; x++){
                    System.out.println("A média final do aluno número " + (x+1) + " é: " + media[x]);
                }
            }
        }   
    }
}
