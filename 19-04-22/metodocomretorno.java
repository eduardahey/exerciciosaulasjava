import java.util.Scanner;
public class metodocomretorno {
    public static Scanner leia = new Scanner(System.in);
    public static float salarioMed(float salario[]){
        float soma = 0;
        float media = 0;
        for(int i = 0; i < salario.length; i++){
            soma += salario[i];
        }
        media = soma/salario.length;
        return media;
    }
    public static float percentual(float salario[]){
        float contador = 0;
        float perc = 0;
        for(int i = 0; i < salario.length; i++){
            if(salario[i] < 150){
                contador += 1;
            }
        }
        perc = (100*contador)/5;
        return perc;
    }
    public static float maior(float salario[]){
        float max = 0;  
        for (int i = 1; i < salario.length; i++) {
            if (salario[i] > max){  
                max = salario[i]; 
            }
        } 
        return max;
    }
    public static float mediafil(float qfilhos[]){
        int somafil = 0;
        float mediafil = 0;
        for(int i = 0; i < qfilhos.length; i++){
            somafil += qfilhos[i];
            mediafil = somafil/qfilhos.length;
        }
        return mediafil;
    }
    public static void main(String[] args){
        float salarios[] = new float[5]; 
        float filhos[] = new float[5]; 
        for(int i = 0; i < 5; i++){
            System.out.print("Informe o salario da " + (i+1) + "ª pessoa:");
            salarios[i] = leia.nextFloat();
            System.out.print("Informe a quantidade de filhos da " + (i+1) + "ª pessoa:");
            filhos[i] = leia.nextFloat();
        }
        System.out.println("A media foi:" + salarioMed(salarios));
        System.out.println("O percentual de pessoas com salário inferior a R$150 é de:" + percentual(salarios) + "%");
        System.out.println("O maior salário é: " + maior(salarios));
        System.out.println("A média de filhos é de:" + mediafil(filhos));
    }
}