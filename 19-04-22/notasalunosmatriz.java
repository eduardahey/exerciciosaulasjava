import java.util.Scanner;
public class notasalunosmatriz {
    public static Scanner leia = new Scanner(System.in);
    public static void main(String[] args){
        double notas[][] = new double[5][3];
        for(int i = 0; i < notas.length; i++){
            for(int j = 0; j < notas[i].length; j++){
                if(j < 2){
                    if(j == 0){
                        System.out.print("Digite a 1ª nota do aluno " + (i+1) + ":");
                    }else if(j == 1){
                        System.out.print("Digite a 2ª nota do aluno " + (i+1) + ":");
                    }
                    notas[i][j] = leia.nextDouble();
                    while(notas[i][j] < 0 || notas[i][j] > 10){
                        System.out.println("Nota digitado inválida.");
                        System.out.print("Digite Novamente:");
                        notas[i][j] = leia.nextDouble();
                    }
                }else{
                    notas[i][j] = (notas[i][0] + notas[i][1])/2;
                }
            }
        }
        for(int i = 0; i < notas.length; i++){
            System.out.println("A média final do aluno " + (i+1) + ": " + notas[i][2]);
        }
    } 
}