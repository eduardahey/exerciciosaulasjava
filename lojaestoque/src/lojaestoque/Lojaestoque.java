package lojaestoque;
import funcionalidades.Modulo;
import java.util.Scanner;
import java.util.ArrayList;
public class Lojaestoque {
    public static Scanner leia = new Scanner(System.in);
    public static void main(String[] args){ 
        ArrayList<Float> valtotal = new ArrayList<>();
        ArrayList<String> reporE = new ArrayList<>();
        String resp = "S";
        
        while(resp.equals("S")){
        System.out.print("Informe o nome da mercadoria:");
        String nome = leia.next();
        System.out.print("Informe a quantidade mínima dessa mercadoria: ");
        int qmin = leia.nextInt();
        System.out.print("Informe a quantidade atual da mercadoria: " );
        int qatual = leia.nextInt();
        System.out.print("Informe a quantidade máxima: ");
        int qmax = leia.nextInt();        
            if(qatual > qmax){
                System.out.println("Você não pode receber uma quantidade maior do que a capacidade máxima.");
                break;                   
            }              
        System.out.print("Informe o valor unitário:");
        float valuni = leia.nextFloat();
            if(qmin <= 0 || qatual <= 0 || qmax <= 0){
                System.out.print("As informações devem ser maiores que 0.");
                break;
            }
        float valm = qatual*valuni;
        valtotal.add(valm);
        if(qatual < qmin ){
            reporE.add(nome);
        }
        System.out.print("MAIS MERCADORIAS?(S/N)");
        resp = leia.next();
        if(resp.equals("N")){
            String merc = String.join(",", reporE);
            System.out.println("O valor total em estoque é de:" + Modulo.soma(valtotal));
            System.out.println("As mercadorias que precisam ser repostas são: " + merc);
            break;
            }
        }
    }
}
