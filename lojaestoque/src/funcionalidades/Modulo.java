package funcionalidades;
import java.util.List;
public class Modulo {
    public static float soma(List<Float> vt){
        float soma = 0;
        for(int i = 0; i < vt.size(); i++) {
            soma += vt.get(i);
        }
        return soma;
    }
}
